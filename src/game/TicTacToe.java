package game;

import java.util.Scanner;

public class TicTacToe extends Game{
    private int[][] board;
    private int turnPlayer = -1;
    private final char[] markers = {' ','X','O'};
    private Scanner scanner;
    private boolean tieEnd;

    @Override
    void initializeGame() {
        if (playersCount != 2) throw new IllegalArgumentException("Game only supports two players.");
        scanner = new Scanner(System.in);
        //initialize game board data
        board = new int[3][3];
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board[0].length; y++) {
                board[y][x] = 0;
            }
        }
    }

    @Override
    void makePlay(int player) {
        turnPlayer = player;
        printBoard();
        printSelect();

        int[] coords;

        while (true){
            System.out.print("Selection: ");
            int selected = Integer.parseInt(scanner.nextLine());
            if (selected >= 1 && selected <= 9){
                coords = XYformI(selected - 1, board.length, board[0].length);
                if (board[coords[1]][coords[0]] == 0) break;
            }
            System.out.println("Invalid selection");
        }
        System.out.println("-".repeat(12));

        board[coords[1]][coords[0]] = turnPlayer + 1;
    }

    @Override
    boolean endOfGame() {
        boolean hasWinner = false;
        boolean tie = true;
        for (int x = 0; x < board.length; x++) {
            hasWinner |= ((board[0][x] != 0) && (board[0][x] == board[1][x]) && (board[0][x] == board[2][x]));
        }
        for (int y = 0; y < board[0].length; y++) {
            hasWinner |= ((board[y][0] != 0) && (board[y][0] == board[y][1]) && (board[y][0] == board[y][2]));
        }
        hasWinner |= ((board[0][0] != 0) && (board[0][0] == board[1][1]) && (board[0][0] == board[2][2]));
        hasWinner |= ((board[0][2] != 0) && (board[0][2] == board[1][1]) && (board[0][2] == board[2][0]));

        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board[0].length; y++) {
                tie &= board[y][x] != 0;
            }
        }
        tieEnd = tie;

        return hasWinner || tie;
    }

    @Override
    void printWinner() {
        printBoard();
        if (!tieEnd) System.out.printf("%c wins!", markers[turnPlayer + 1]);
        else System.out.print("Tie!");
    }

    private void printBoard(){
        StringBuilder boardString = new StringBuilder();
        for (int x = 0; x < board.length * 2 + 1; x++) {
            for (int y = 0; y < board[0].length * 2 + 1; y++) {
                if(x % 2 == 0 && y % 2 == 0){
                    //corners
                         if(x == 0 && y == 0)                                   boardString.append('┌');
                    else if(x == board.length * 2 && y == 0)                    boardString.append('└');
                    else if(x == 0 && y == board[0].length * 2)                 boardString.append('┐');
                    else if(x == board.length * 2 && y == board[0].length * 2)  boardString.append('┘');
                    //edges
                    else if(x == 0)                                             boardString.append('┬');
                    else if(x == board.length * 2)                              boardString.append('┴');
                    else if(y == 0)                                             boardString.append('├');
                    else if(y == board[0].length * 2)                           boardString.append('┤');
                    //interior
                    else                                                        boardString.append('┼');
                }
                else if (x % 2 == 0){
                    boardString.append('─');
                }
                else if (y % 2 == 0){
                    boardString.append('│');
                }
                else {
                    boardString.append(markers[board[(x/2)][y/2]]);
                }
            }
            boardString.append('\n');
        }
        System.out.print(boardString);
    }

    private void printSelect(){
        StringBuilder selection = new StringBuilder();
        selection.append("%c's turn.%n".formatted(markers[turnPlayer + 1]));
        selection.append("Select space.\n");
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board[0].length; y++) {
                if (board[x][y] == 0) {
                    selection.append(1 + (y + board.length * x));
                } else {
                    selection.append('-');
                }
                selection.append(' ');
            }
            selection.append('\n');
        }
        System.out.print(selection);
    }

    private int[] XYformI(int i, int w, int h){
        return new int[] {i%w,i/h};
    }
}